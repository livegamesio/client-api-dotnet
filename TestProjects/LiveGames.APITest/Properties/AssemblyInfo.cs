﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("LiveGames.APITest")]
[assembly: AssemblyDescription("LiveGames Client API - Test Project")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("LiveTech N.V")]
[assembly: AssemblyProduct("LiveGames.ClientLib")]
[assembly: AssemblyCopyright("LiveTech N.V (livegames.io) - 2016")]
[assembly: AssemblyTrademark("LiveTech N.V (livegames.io)")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("1.0.*")]

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]