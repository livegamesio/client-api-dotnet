﻿using System;
using LiveGames.Client;

namespace LiveGames.APITest {
	class MainClass {
		public static void Main(string[] args) {

			var apiKey = "YOUR_API_KEY";
			var apiSecret = "YOUR_API_SECRET";

			var user = new LGUser("USERID", "USER_PARENTID");

			var api = new API(user, apiKey, apiSecret);
			var usr = api.checkUser();
			var roomPot = api.jackpot("room1");
			if (usr != null) {
				Console.WriteLine("UserId: {0} - LGId:D {1}", usr.RemotePk, usr.Id);
			}
			Console.WriteLine("Room1 Pot: {0}", roomPot);

		}
	}
}
