﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using LiveGames.Client;

namespace LiveGames.WebsiteIntegration.Controllers {
	public class HomeController : Controller {
		public ActionResult Index() {

			var apiKey = "YOUR_API_KEY";
			var apiSecret = "YOUR_API_SECRET";

			var user = new LGUser("USERID", "USER_PARENTID");
			var api = new API(user, apiKey, apiSecret);

			ViewData["sign"] = api.MakeUserJWT();

			return View();
		}
	}
}
