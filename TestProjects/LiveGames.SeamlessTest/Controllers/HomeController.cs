﻿using System.Web.Mvc;
using LiveGames.Client;

namespace LiveGames.SeamlessTest.Controllers {
	public class HomeController : Controller {
		public ActionResult Index() {

			var apiSecret = "YOUR_API_SECRET";

			var handler = new Handler(Request.Params, apiSecret);

			if (handler.Action == HandlerActions.GetWallet) {


				var getWalletReq = GetWalletRequest.ParseFromRequest(handler.Data);
				var u1 = getWalletReq.UserId;


				handler.SetResponse(new GetWalletResponse() {
					Credit = 100
				});

			} else if (handler.Action == HandlerActions.UpdateWallet) {

				var updateReq = UpdateWalletRequest.ParseFromRequest(handler.Data);
				var u1 = updateReq.UserId;
				var amount = updateReq.Amount;

				handler.SetResponse(new UpdateWalletResponse() {
					ResponseId = "clientTransactionId",
					Credit = 100
				});

			} else if (handler.Action == HandlerActions.Ping) {
				handler.SetResponse(true);
			}

			return Json(handler.GetResponse(), JsonRequestBehavior.AllowGet);

		}
	}
}

