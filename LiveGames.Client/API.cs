﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LiveGames.Client {
	public class API {

		private string apiUrl = "http://api.livegames.io/api";
		private int expire = 86400;

		private string apiKey = null;
		private string apiSecret = null;
		private LGUser user;

		public API(LGUser user, string apiKey = null, string apiSecret = null, string apiUrl = null) {

			if (user == null)
				throw new Exception("USER_OBJECT_NOT_FOUND");

			this.user = user;

			if (!String.IsNullOrEmpty(apiKey))
				this.apiKey = apiKey;

			if (!String.IsNullOrEmpty(apiSecret))
				this.apiSecret = apiSecret;

			if (!String.IsNullOrEmpty(apiUrl))
				this.apiUrl = apiUrl;

			if (String.IsNullOrEmpty(this.apiKey))
				this.apiKey = ConfigurationManager.AppSettings["LiveGames.apiKey"];

			if (String.IsNullOrEmpty(this.apiSecret))
				this.apiSecret = ConfigurationManager.AppSettings["LiveGames.apiSecret"];

			var _apiUrl = ConfigurationManager.AppSettings["LiveGames.apiUrl"];
			if (!String.IsNullOrEmpty(_apiUrl)) {
				this.apiUrl = _apiUrl;
			}

			if (user == null || String.IsNullOrEmpty(this.apiKey) || String.IsNullOrEmpty(this.apiSecret) || String.IsNullOrEmpty(this.apiUrl))
				throw new Exception("INVALID_CONFIG");
		}

		/**
	 * Kullanıcı var mı yok mu kontrol eder.
	 *
	 * @return mixed
	 */
		public LGUser checkUser() {
			dynamic response = this.callRequest("v1/check");

			if (!this.hasResponseError(response) && response != null && response.payload != null && response.payload.user != null) {
				var usr = this.parseUser(response.payload.user);
				if (response.payload.wallet != null) {
					usr.Wallet = this.parseWallet(response.payload.wallet);
				}
				return usr;
			}
			return null;
		}

		/**
		 * Kullanıcının son bakiyesini döndürür.
		 *
		 * @return mixed
		 */
		public LGWallet getWallet() {
			dynamic response = this.callRequest("v1/wallet");
			if (response != null && response.payload != null) {
				return this.parseWallet(response.payload);
			}
			return null;
		}


		/**
		 * Yeni Kullanıcı yaratır.
		 *
		 * UserData Keys =>
		 *  - id
		 *  - parent
		 *  - name
		 *  - surname
		 *  - email
		 *  - phone
		 * @return mixed
		 * @throws \Exception
		 */
		public LGUser createUser() {
			dynamic response = this.callRequest("v1/create");
			if (!this.hasResponseError(response) && response != null && response.payload != null && response.payload.user != null) {
				var usr = this.parseUser(response.payload.user);
				return usr;
			}
			return null;
		}

		/**
		 * Verilen Parent kullanıcısından bakiye çekilerek diğer kullanıcıya aktarılır.
		 *
		 * @param $amount
		 * @return mixed
		 * @throws \Exception
		 */
		public TransferResponse deposit(float amount) {
			var req = new Dictionary<string, string>() {
				{ "amount", amount.ToString() }
			};
			var response = this.callRequest("v1/deposit", req);
			return this.parseTransferResponse(response);
		}

		/**
		 * Kullanıcının cüzdanından bakiye düşülecektir.
		 *
		 * @param $amount
		 * @return mixed
		 * @throws \Exception
		 */
		public TransferResponse withdraw(float amount) {
			var req = new Dictionary<string, string>() {
				{ "amount", amount.ToString() }
			};
			var response = this.callRequest("v1/withdraw", req);
			return this.parseTransferResponse(response);
		}


		public float jackpot(string room = null) {
			var req = new Dictionary<string, string>() {
				{ "room", room }
			};
			dynamic response = this.callRequest("v1/jackpot", req);
			return response != null && response.payload != null && response.payload.pot != null ? (float)response.payload.pot : 0;
		}


		/**
		 * Son kazananlar listesi
		 *
		 *  postArray Params
		 *    "crit" => ["totalWon" => "> 10", "lastWon" => "> 10"]
		 *    "sort" => ["updatedAt" => "desc"]
		 *    "limit" => 10
		 *    "skip" => 0
		 *
		 * @param $q TODO
		 * @return mixed
		 */
		public object lastWinners() {
			//TODO: Objeden modele çevrilecek
			return this.callRequest("v1/lastWinners");
		}

		/**
		 * En Çok Kazananlar listesi
		 *
		 *  postArray Params
		 *    "crit" => ["totalWon" => "> 10", "lastWon" => "> 10"]
		 *    "sort" => ["updatedAt" => "desc"]
		 *    "limit" => 10
		 *    "skip" => 0
		 *
		 * @param $q
		 * @return mixed
		 */
		public object mostWinners() {
			//TODO: Objeden modele çevrilecek
			return this.callRequest("v1/mostWinners");
		}

		public object mostWinnerNumbers() {
			//TODO: Objeden modele çevrilecek
			return this.callRequest("v1/mostWinnerNumbers");
		}

		public object mostDrawnNumbers() {
			//TODO: Objeden modele çevrilecek
			return this.callRequest("v1/mostDrawnNumbers");
		}

		public object mostWinnerCards() {
			//TODO: Objeden modele çevrilecek
			return this.callRequest("v1/mostWinnerCards");
		}


		private object callRequest(string url, Dictionary<string, string> postContent = null) {

			try {
				if (postContent == null)
					postContent = new Dictionary<string, string>();

				postContent.Add("token", MakeUserJWT());

				return AsyncHelper.RunSync<object>(() => this.makeRequest(url, postContent));
			} catch (Exception ex) {
				throw ex;
			}

		}

		public string MakeUserJWT() {
			var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			var now = (int)Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);

			var payload = new Token() {
				Game = GameType.Tombala,
				ApiKey = this.apiKey,

				User = this.user,

				IssuedAt = now,
				//NotBefore = now - 1,
				Expire = now + this.expire,

				Jti = Guid.NewGuid().ToString().Substring(0, 8),
			};
			var token = Tokenizer.Sign(payload, this.apiSecret);
			return token;
		}

		private async Task<object> makeRequest(string url, Dictionary<string, string> requestContent) {
			using (var client = new HttpClient()) {
				var content = new FormUrlEncodedContent(requestContent);
				var response = await client.PostAsync(String.Format("{0}/{1}", this.apiUrl, url), content);
				var responseString = await response.Content.ReadAsStringAsync();

				if (response.StatusCode == System.Net.HttpStatusCode.OK) {
					try {
						var responseObject = JsonConvert.DeserializeObject(responseString);
						return responseObject;
					} catch (Exception ex) {
						throw ex;
					}
				} else {
					//TODO: !!! Exc.
					throw new Exception("SERVER_ERROR");
				}
			}
		}

		private bool hasResponseError(dynamic response) {
			return (response != null && response.errors != null && response.errors != false);
		}

		private List<ApiError> throwResponseError(dynamic response) {
			var fields = new List<ApiError>();

			if (response != null && response.errors != null && response.errors != false) {
				var serialized = JsonConvert.SerializeObject(response.errors);
				var test = new List<ApiError>();
				fields = JsonConvert.DeserializeAnonymousType(serialized, test);
			}
			return fields;
		}

		private LGUser parseUser(dynamic user) {
			if (user != null) {
				var tusr = new LGUser();
				var serialized = JsonConvert.SerializeObject(user);
				LGUser usr = JsonConvert.DeserializeAnonymousType(serialized, tusr);
				return usr;
			}
			return null;
		}

		private LGWallet parseWallet(dynamic wallet) {
			if (wallet != null && wallet["credit"] != null) {
				return new LGWallet(wallet["credit"].Value, wallet["commission"].Value, wallet["penalty"].Value);
			}
			return null;
		}

		private TransferResponse parseTransferResponse(dynamic response) {
			if (response != null && response.payload != null) {
				var r = new TransferResponse();
				if (response.payload.withdraw != null && response.payload.withdraw.transaction != null) {
					r.Withdraw = Transaction.ParseFromResponse(response.payload.withdraw.transaction);
				}
				if (response.payload.deposit != null && response.payload.deposit.transaction != null) {
					r.Deposit = Transaction.ParseFromResponse(response.payload.deposit.transaction);
				}
				return r;
			}
			return null;
		}

	}
}

