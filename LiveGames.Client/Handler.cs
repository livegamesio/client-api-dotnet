﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace LiveGames.Client {
	public class Handler {

		public string Action { get; set; }
		public IDictionary<string, string> Data { get; set; }
		public string SecurityKey { get; set; }
		public bool IsSecureRequest { get; set; }

		private string _dataString;
		private string _apiSecret;
		private Dictionary<string, string> _response = null;


		public Handler(NameValueCollection formData, string apiSecret = null) {
			this._apiSecret = apiSecret;
			if (String.IsNullOrEmpty(apiSecret))
				this._apiSecret = ConfigurationManager.AppSettings["LiveGames.apiSecret"];

			this.Action = formData.Get("lgAction");
			this._dataString = formData.Get("lgData");
			this.SecurityKey = formData.Get("lgSec");


			if (String.IsNullOrEmpty(this.SecurityKey)) {
				this.Data = Tokenizer.Decode<IDictionary<string, string>>(this._dataString, this._apiSecret);
				this.IsSecureRequest = true;
			} else {
				this.Data = JsonConvert.DeserializeObject<IDictionary<string, string>>(this._dataString);
				this.IsSecureRequest = ValidateSecurityKey(this.SecurityKey, this.Data);
			}

		}

		public object SetResponse<T>(T payload, List<string> errors = null) {
			var token = new SeamlessResponse() {
				Payload = payload,
				Errors = errors
			};
			var jwt = Tokenizer.Sign(token, this._apiSecret);

			this._response = new Dictionary<string, string>() {
				{ "response", jwt }
			};

			return this._response;
		}

		public object SetError(string errorMessage) {
			var token = new SeamlessResponse() {
				Payload = null,
				Errors = new List<string>() { errorMessage }
			};
			var jwt = Tokenizer.Sign(token, this._apiSecret);

			this._response = new Dictionary<string, string>() {
				{ "response", jwt }
			};
			return this._response;
		}


		public bool ValidateSecurityKey(string secKey, IDictionary<string, string> data) {
			try {
				var sorted = data.Keys.ToList();
				var values = new List<string>();
				sorted.Sort();
				foreach (var k in sorted) {
					values.Add(data[k]);
				}
				values.Add(this._apiSecret);
				var nSec = string.Join(".", values);
				return this.MD5(nSec) == secKey;
			} catch (Exception ex) {
				return false;
			}
		}


		public object GetResponse() {
			return this._response;
		}

		public object GetResponseString() {
			return JsonConvert.SerializeObject(this._response);
		}

		public string MD5(string s) {
			using (var provider = System.Security.Cryptography.MD5.Create()) {
				var builder = new StringBuilder();
				foreach (byte b in provider.ComputeHash(Encoding.UTF8.GetBytes(s)))
					builder.Append(b.ToString("x2").ToLower());

				return builder.ToString();
			}
		}

	}
}