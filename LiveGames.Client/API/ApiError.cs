﻿using Newtonsoft.Json;

namespace LiveGames.Client {
	public class ApiError {
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("message")]
		public string Message { get; set; }
	}
}

