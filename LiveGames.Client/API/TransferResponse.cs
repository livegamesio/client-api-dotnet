﻿namespace LiveGames.Client {
	public class TransferResponse {
		public Transaction Deposit { get; set; }
		public Transaction Withdraw { get; set; }
	}
}

