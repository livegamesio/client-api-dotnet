﻿using System.Collections.Generic;

namespace LiveGames.Client {
	public class ApiResponse {
		public object Payload { get; set; }
		public List<ApiError> Errors { get; set; }

		public ApiResponse(dynamic response) {
			this.Payload = response.payload;
		}
	}
}

