﻿using Newtonsoft.Json;

namespace LiveGames.Client {
	public class GetWalletRequest {
		[JsonProperty("uid")]
		public string UserId { get; set; }

		[JsonProperty("apiKey")]
		public string ApiKey { get; set; }

		[JsonProperty("game")]
		public string Game { get; set; }

		[JsonProperty("exp")]
		public int Expire { get; set; }

		[JsonProperty("nbf")]
		public int NotBefore { get; set; }

		[JsonProperty("iat")]
		public int IssuedAt { get; set; }

		[JsonProperty("jti")]
		public string Jti { get; set; }

		[JsonProperty("ts")]
		public string TS { get; set; }

		public static GetWalletRequest ParseFromRequest(dynamic req) {
			if (req == null)
				return null;
			var serialized = JsonConvert.SerializeObject(req);
			return JsonConvert.DeserializeAnonymousType(serialized, new GetWalletRequest());
		}
	}
}

