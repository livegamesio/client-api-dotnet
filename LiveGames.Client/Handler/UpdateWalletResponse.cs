﻿using Newtonsoft.Json;

namespace LiveGames.Client {
	public class UpdateWalletResponse {

		[JsonProperty("id")]
		public string ResponseId { get; set; }

		[JsonProperty("credit")]
		public double Credit { get; set; }
	}
}

