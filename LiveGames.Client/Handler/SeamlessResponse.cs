﻿using Newtonsoft.Json;

namespace LiveGames.Client {
	public class SeamlessResponse {

		[JsonProperty("payload")]
		public object Payload { get; set; }

		[JsonProperty("errors")]
		public object Errors { get; set; }

		[JsonProperty("sec")]
		public string SecurityKey { get; set; }
	}
}