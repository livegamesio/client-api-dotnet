﻿using Newtonsoft.Json;

namespace LiveGames.Client {
	public class GetWalletResponse {
		[JsonProperty("credit")]
		public double Credit { get; set; }
	}
}

