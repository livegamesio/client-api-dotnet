﻿using Newtonsoft.Json;

namespace LiveGames.Client {
	public class LGWallet {

		[JsonProperty("credit")]
		public double Credit { get; set; }

		[JsonProperty("commission")]
		public double Commission { get; set; }

		[JsonProperty("penalty")]
		public double Penalty { get; set; }

		public LGWallet() {
		}

		public LGWallet(double credit = 0, double commission = 0, double penalty = 0) {
			this.Credit = credit;
			this.Commission = commission;
			this.Penalty = penalty;
		}
	}
}

