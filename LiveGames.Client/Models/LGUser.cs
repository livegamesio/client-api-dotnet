﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LiveGames.Client {
	public class LGUser {

		public LGUser() { }

		public LGUser(string RemotePk, string ParentId, string Name = null, string Surname = null, string Email = null, string Password = null, string Phone = null) {
			if (String.IsNullOrEmpty(RemotePk) || String.IsNullOrEmpty(ParentId))
				throw new Exception("INVALID_USER");

			this.RemotePk = RemotePk;
			this.ParentId = ParentId;
			this.Name = Name;
			this.Surname = Surname;
			this.Email = Email;
			this.Password = Password;
			this.Phone = Phone;
		}

		[JsonProperty("id")]
		public string Id { get; }

		[JsonProperty("remotePk")]
		public string RemotePk { get; set; }

		[JsonProperty("parent")]
		public string ParentId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("surname")]
		public string Surname { get; set; }

		[JsonProperty("phone")]
		public string Phone { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("password")]
		public string Password { get; set; }

		//beta props
		[JsonProperty("userType")]
		public string UserType { get; set; }

		[JsonProperty("exp")]
		public string Exp { get; set; }

		[JsonProperty("flag")]
		public string Flag { get; set; }

		//extras
		[JsonIgnore]
		public LGWallet Wallet { get; set; }


		public IDictionary<string, string> ToCreateObject() {
			var d = new Dictionary<string, string>() {
				{"remotePk",this.RemotePk},
				{"parent",this.ParentId},
			};

			if (!String.IsNullOrEmpty(this.Name))
				d.Add("name", this.Name);

			if (!String.IsNullOrEmpty(this.Surname))
				d.Add("surname", this.Surname);

			if (!String.IsNullOrEmpty(this.Phone))
				d.Add("phone", this.Phone);

			if (!String.IsNullOrEmpty(this.Email))
				d.Add("email", this.Email);

			if (!String.IsNullOrEmpty(this.Password))
				d.Add("password", this.Password);

			return d;
		}

	}
}