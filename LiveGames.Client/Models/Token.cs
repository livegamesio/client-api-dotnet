﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;

namespace LiveGames.Client {
	public class Token {

		[JsonProperty("user")]
		public LGUser User { get; set; }

		[JsonProperty("apiKey")]
		public string ApiKey { get; set; }

		[JsonProperty("game")]
		public string Game { get; set; }

		[JsonProperty("exp")]
		public int Expire { get; set; }

		//[JsonProperty("nbf")] --- Timezone farkliligi sebebiyle kaldirildi.
		//public int NotBefore { get; set; }

		[JsonProperty("iat")]
		public int IssuedAt { get; set; }

		[JsonProperty("jti")]
		public string Jti { get; set; }


		public Dictionary<string, object> ToDict() {
			//kullanılmayabilir...
			return this.GetType()
	 					.GetProperties(BindingFlags.Instance | BindingFlags.Public)
					   .ToDictionary(prop => prop.Name, prop => prop.GetValue(this, null));

		}
	}
}