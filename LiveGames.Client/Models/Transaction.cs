﻿using Newtonsoft.Json;

namespace LiveGames.Client {
	public class Transaction {

		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("game")]
		public string Game { get; set; }

		[JsonProperty("jsonObject")]
		public object JsonObject { get; set; }

		[JsonProperty("amount")]
		public double Amount { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("flag")]
		public int Flag { get; set; }

		[JsonProperty("remoteFlag")]
		public int RemoteFlag { get; set; }

		[JsonProperty("userId")]
		public string UserId { get; set; }

		[JsonProperty("remoteId")]
		public string RemoteId { get; set; }

		[JsonProperty("objectType")]
		public string ObjectType { get; set; }

		[JsonProperty("createdBy")]
		public string CreatedBy { get; set; }

		[JsonProperty("createdAt")]
		public string CreatedAt { get; set; }

		[JsonProperty("updatedAt")]
		public string UpdatedAt { get; set; }

		[JsonProperty("sessionId")]
		public string SessionId { get; set; }

		[JsonProperty("roomId")]
		public string RoomId { get; set; }

		[JsonProperty("objectId")]
		public string ObjectId { get; set; }

		[JsonProperty("responseId")]
		public string ResponseId { get; set; }

		public static Transaction ParseFromResponse(dynamic transactionObject) {
			if (transactionObject == null)
				return null;

			var serialized = JsonConvert.SerializeObject(transactionObject);
			return JsonConvert.DeserializeAnonymousType(serialized, new Transaction());
		}
	}
}

