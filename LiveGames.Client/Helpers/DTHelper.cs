﻿using System;
namespace LiveGames.Client {
	public static class DTHelper {
		public static int GetTimestamp(this DateTime dt) {
			var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return (int)Math.Round((dt - unixEpoch).TotalSeconds);
		}
	}
}

