﻿using System;

namespace LiveGames.Client {
	public static class Tokenizer {

		public static string Sign<T>(T payload, string secret) {
			try {
				var s = Newtonsoft.Json.JsonConvert.SerializeObject(payload,
																	Newtonsoft.Json.Formatting.Indented,
																	new Newtonsoft.Json.JsonSerializerSettings { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore });
				var d = JWT.JsonWebToken.JsonSerializer.Deserialize<object>(s);
				var t = JWT.JsonWebToken.Encode(d, secret, JWT.JwtHashAlgorithm.HS256);
				return t;
			} catch (Exception ex) {
				throw ex;
			}
		}

		public static T Decode<T>(string token, string secret) {
			try {
				var jsp = JWT.JsonWebToken.DecodeToObject<T>(token, secret, true);
				if (jsp == null) {
					throw new JWT.SignatureVerificationException("INVALID_TOKEN");
				}
				return jsp;
			} catch (Exception ex) {
				throw ex;
			}
		}

	}
}

