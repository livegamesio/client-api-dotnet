﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("LiveGames.Client")]
[assembly: AssemblyDescription("LiveGames Client API and Handler")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Livetechs N.V")]
[assembly: AssemblyProduct("LiveGames.ClientLib")]
[assembly: AssemblyCopyright("Livetechs N.V (livegames.io) - 2016")]
[assembly: AssemblyTrademark("Livetechs N.V (livegames.io)")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("1.0.*")]

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]

