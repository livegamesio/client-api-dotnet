﻿namespace LiveGames.Client {
	public struct HandlerActions {
		public static readonly string GetWallet = "GetWallet";
		public static readonly string UpdateWallet = "UpdateWallet";
		public static readonly string Ping = "Ping";
	}
}